package calculadora;

import java.util.Scanner;

public class Calculadora {

    static int num1, num2;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        System.out.println("Entra primer num: ");
        boolean ok = true;

        do {
            try {
                num1 = sc.nextInt();
                ok = true;
            } catch (Exception e) {
                System.out.println("no es un numero");
                ok = false;
            sc.nextLine();
            }
        } while (ok == false);

        System.out.println("Entra segon num: ");

        do {
            try {
                num2 = sc.nextInt();
                ok = true;
            } catch (Exception e) {
                System.out.println("no es un numero");
                ok = false;
                sc.next();
            }
        } while (ok == false || num2==0);
               

        
        System.out.println("Resultado suma: " + suma(num1, num2));
        System.out.println("Resultado resta: " + resta(num1, num2));
        System.out.println("Resultado multiplicació: " + multi(num1, num2));
        
        System.out.println("Resultado divisió: " + divisio(num1, num2));

    }

    private static int suma(int num1, int num2) {

        return num1 + num2;

    }

    private static int resta(int num1, int num2) {

        return num1 - num2;

    }

    private static int multi(int num1, int num2) {

        return num1 * num2;

    }

    private static int divisio(int num1, int num2) {

        return num1 / num2;

    }

}
